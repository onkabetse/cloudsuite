<!doctype html>
<html class="no-js" lang="en">
    <head>
        <!-- title -->
        <title></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <meta name="author" content="ThemeZaa">
        <!-- description -->
        <meta name="description" content="POFO is a highly creative, modern, visually stunning and Bootstrap responsive multipurpose agency and portfolio HTML5 template with 25 ready home page demos.">
        <!-- keywords -->
        <meta name="keywords" content="creative, modern, clean, bootstrap responsive, html5, css3, portfolio, blog, agency, templates, multipurpose, one page, corporate, start-up, studio, branding, designer, freelancer, carousel, parallax, photography, personal, masonry, grid, coming soon, faq">
        <!-- favicon -->
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
        <!-- animation -->
        <link rel="stylesheet" href="css/animate.css" />
        <!-- bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <!-- et line icon --> 
        <link rel="stylesheet" href="css/et-line-icons.css" />
        <!-- font-awesome icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <!-- themify icon -->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- swiper carousel -->
        <link rel="stylesheet" href="css/swiper.min.css">
        <!-- justified gallery  -->
        <link rel="stylesheet" href="css/justified-gallery.min.css">
        <!-- magnific popup -->
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <!-- revolution slider -->
        <link rel="stylesheet" type="text/css" href="revolution/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
        <!-- bootsnav -->
        <link rel="stylesheet" href="css/bootsnav.css">
        <!-- style -->
        <link rel="stylesheet" href="css/style.css" />
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css" />
        <!--[if IE]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>   
        <!-- start header -->
		<?php $active = "contactus"; include 'php-components/header.php';?>
        <!-- end header -->
        <!-- start page title section -->
        <section class="wow fadeIn bg-light-gray padding-35px-tb page-title-small top-space">
            <div class="container">
                <div class="row equalize xs-equalize-auto">
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 display-table">
                        <div class="display-table-cell vertical-align-middle text-left xs-text-center">
                            <!-- start page title -->
                            <h1 class="alt-font text-extra-dark-gray font-weight-600 no-margin-bottom text-uppercase">Websites</h1>
                            <!-- end page title -->
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 display-table text-right xs-text-left xs-margin-10px-top">
                        <div class="display-table-cell vertical-align-middle breadcrumb text-small alt-font">
                            <!-- start breadcrumb -->
                            <ul class="xs-text-center">
                                <li><a href="#" class="text-dark-gray">Packages</a></li>
                                <li><a href="#" class="text-dark-gray">Support</a></li>
                                <li class="text-dark-gray">Get in touch</li>
                            </ul>
                            <!-- end breadcrumb -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end page title section -->
        <!-- start feature box section -->
        <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-8 col-xs-12 center-col margin-eight-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
                        <div class="alt-font text-medium-gray margin-10px-bottom text-uppercase text-small">Unlimited customization possibilities</div>
                        <h5 class="alt-font text-extra-dark-gray font-weight-600">Pixel perfect design and clear code delivered to your projects</h5>
                    </div>
                </div>
                <div class="row">
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-30px-bottom wow fadeInUp last-paragraph-no-margin xs-text-center">
                        <div class="margin-ten-bottom overflow-hidden image-hover-style-1 sm-margin-20px-bottom">
                            <a href="services-modern.html"><img src="http://placehold.it/750x500" alt=""/></a>
                        </div>
                        <a href="services-modern.html" class="alt-font margin-5px-bottom display-block text-extra-dark-gray font-weight-600 text-uppercase text-small">E-Commerce Solutions</a>
                        <p class="width-95 sm-width-100">Lorem Ipsum is simply text the printing and typesetting standard industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <a href="services-modern.html" class="text-uppercase alt-font text-extra-dark-gray font-weight-600 text-extra-small">View E-Commerce Solutions <i class="fas fa-long-arrow-alt-right margin-5px-left text-deep-pink text-medium position-relative top-2" aria-hidden="true"></i></a>
                    </div>
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-4 col-xs-12 xs-margin-30px-bottom wow fadeInUp last-paragraph-no-margin xs-text-center" data-wow-delay="0.2s">
                        <div class="margin-ten-bottom overflow-hidden image-hover-style-1 sm-margin-20px-bottom">
                            <a href="services-modern.html"><img src="http://placehold.it/750x500" alt=""/></a>
                        </div>
                        <a href="services-modern.html" class="alt-font margin-5px-bottom display-block text-extra-dark-gray font-weight-600 text-uppercase text-small">Web Development</a>
                        <p class="width-95 sm-width-100">Lorem Ipsum is simply text the printing and typesetting standard industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <a href="services-modern.html" class="text-uppercase alt-font text-extra-dark-gray font-weight-600 text-extra-small">View Web Development <i class="fas fa-long-arrow-alt-right margin-5px-left text-deep-pink text-medium position-relative top-2" aria-hidden="true"></i></a>
                    </div>
                    <!-- end feature box item -->
                    <!-- start feature box item -->
                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp last-paragraph-no-margin xs-text-center" data-wow-delay="0.4s">
                        <div class="margin-ten-bottom overflow-hidden image-hover-style-1 sm-margin-20px-bottom">
                            <a href="services-modern.html"><img src="http://placehold.it/750x500" alt=""/></a>
                        </div>
                        <a href="services-modern.html" class="alt-font margin-5px-bottom display-block text-extra-dark-gray font-weight-600 text-uppercase text-small">Marketing Strategy</a>
                        <p class="width-95 sm-width-100">Lorem Ipsum is simply text the printing and typesetting standard industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <a href="services-modern.html" class="text-uppercase alt-font text-extra-dark-gray font-weight-600 text-extra-small">View Marketing Strategy <i class="fas fa-long-arrow-alt-right margin-5px-left text-deep-pink text-medium position-relative top-2" aria-hidden="true"></i></a>
                    </div>                    
                    <!-- end feature box item -->
                </div>
            </div>
        </section>
        <!-- end  feature box section -->
        <!-- start feature box section -->
        <section class="no-padding wow fadeIn bg-extra-dark-gray">
            <div class="container-fluid">
                <div class="row equalize sm-equalize-auto">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cover-background sm-height-500px xs-height-350px wow fadeInLeft"  style="background-image:url('http://placehold.it/1200x683');"><div class="hidden-sm height-350px"></div></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
                        <div class="padding-twelve-all md-padding-ten-all pull-left sm-no-padding-lr xs-padding-50px-tb">
                            <div class="col-2-nth">
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 margin-six-bottom md-margin-30px-bottom last-paragraph-no-margin">
                                    <div class="feature-box-5 position-relative">
                                        <i class="icon-target text-medium-gray icon-medium"></i>
                                        <div class="feature-content">
                                            <div class="text-white margin-5px-bottom alt-font">Branding Design</div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem standard dummy text.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 margin-six-bottom md-margin-30px-bottom last-paragraph-no-margin">
                                    <div class="feature-box-5 position-relative">
                                        <i class="icon-desktop text-medium-gray icon-medium"></i>
                                        <div class="feature-content">
                                            <div class="text-white margin-5px-bottom alt-font">Web Design</div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem standard dummy text.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 margin-six-bottom md-margin-30px-bottom last-paragraph-no-margin">
                                    <div class="feature-box-5 position-relative">
                                        <i class="icon-puzzle text-medium-gray icon-medium"></i>
                                        <div class="feature-content">
                                            <div class="text-white margin-5px-bottom alt-font">Web Development</div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem standard dummy text.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 margin-six-bottom md-margin-30px-bottom last-paragraph-no-margin">
                                    <div class="feature-box-5 position-relative">
                                        <i class="icon-tools text-medium-gray icon-medium"></i>
                                        <div class="feature-content">
                                            <div class="text-white margin-5px-bottom alt-font">Graphics Design</div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem standard dummy text.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 md-margin-30px-bottom last-paragraph-no-margin">
                                    <div class="feature-box-5 position-relative">
                                        <i class="icon-briefcase text-medium-gray icon-medium"></i>
                                        <div class="feature-content">
                                            <div class="text-white margin-5px-bottom alt-font">Social Marketing</div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem standard dummy text.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end feature box item -->
                                <!-- start feature box item -->
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 last-paragraph-no-margin">
                                    <div class="feature-box-5 position-relative ">
                                        <i class="icon-basket text-medium-gray icon-medium"></i>
                                        <div class="feature-content">
                                            <div class="text-white margin-5px-bottom alt-font">e-Commerce Solutions</div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem standard dummy text.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end feature box item -->
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        <!-- end feature box section -->
        <!-- start tabs section -->
        <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-8 col-xs-12 center-col margin-eight-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
                        <div class="alt-font text-medium-gray margin-10px-bottom text-uppercase text-small">The best digital solutions</div>
                        <h5 class="alt-font text-extra-dark-gray font-weight-600">A creative agency specialized in brand strategy and digital creation</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 center-col text-center no-padding tab-style1" id="animated-tab">
                        <!-- start tab navigation -->
                        <ul class="nav nav-tabs margin-50px-bottom xs-no-margin-bottom"> 
                            <li class="nav active"><a href="#tab6_sec1" data-toggle="tab" class="xs-min-height-inherit xs-no-padding"><span><i class="icon-chat icon-medium text-medium-gray margin-10px-bottom display-block"></i></span><span class="alt-font text-medium-gray text-uppercase text-small">01. Discussion</span></a></li>
                            <li class="nav"><a href="#tab6_sec2" data-toggle="tab" class="xs-min-height-inherit xs-no-padding"><span><i class="icon-tools icon-medium text-medium-gray margin-10px-bottom display-block"></i></span><span class="alt-font text-medium-gray text-uppercase text-small">02. Creative Concept</span></a></li>
                            <li class="nav"><a href="#tab6_sec3" data-toggle="tab" class="xs-min-height-inherit xs-no-padding"><span><i class="icon-megaphone icon-medium text-medium-gray margin-10px-bottom display-block"></i></span><span class="alt-font text-medium-gray text-uppercase text-small">03. Production</span></a></li>
                            <li class="nav"><a href="#tab6_sec4" data-toggle="tab" class="xs-min-height-inherit xs-no-padding"><span><i class="icon-heart icon-medium text-medium-gray margin-10px-bottom display-block"></i></span><span class="alt-font text-medium-gray text-uppercase text-small">04. Happy Client</span></a></li>
                        </ul>
                        <!-- end tab navigation -->
                        <div class="tab-content">
                            <!-- start tab content -->
                            <div id="tab6_sec1" class="center-col tab-pane fade in active"> 
                                <div class="tab-pane fade in">
                                    <div class="equalize sm-equalize-auto">
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <img src="http://placehold.it/900x650" alt=""/>
                                        </div>
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 border-all border-width-2 border-color-extra-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-medium font-weight-600 alt-font text-uppercase margin-10px-bottom display-block text-extra-dark-gray">01. Discussion</span>
                                                    <span class="alt-font text-medium-gray text-medium">Lorem Ipsum is simply dummy text of the printing and industry. Lorem Ipsum typesetting has dummy text.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-left">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 bg-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-extra-dark-gray alt-font margin-10px-bottom display-inline-block text-medium">Don’t worry, you’re in safe hands</span>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text.</p>
                                                    <a href="about-us-classic.html" class="btn btn-dark-gray btn-very-small text-extra-small">About Company</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div id="tab6_sec2" class="center-col tab-pane fade in"> 
                                <div class="tab-pane fade in"> 
                                    <div class="equalize sm-equalize-auto">
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <img src="http://placehold.it/900x650" alt=""/>
                                        </div>
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 border-all border-width-2 border-color-extra-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-medium font-weight-600 alt-font text-uppercase margin-10px-bottom display-block text-extra-dark-gray">02. Creative Concept</span>
                                                    <span class="alt-font text-medium-gray text-medium">Lorem Ipsum is simply dummy text of the printing and industry. Lorem Ipsum typesetting has dummy text.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-left">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 bg-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-extra-dark-gray alt-font margin-10px-bottom display-inline-block text-medium">We are a service digital agency</span>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text.</p>
                                                    <a href="about-us-classic.html" class="btn btn-dark-gray btn-very-small text-extra-small">About Company</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div id="tab6_sec3" class="center-col tab-pane fade in"> 
                                <div class="tab-pane fade in"> 
                                    <div class="equalize sm-equalize-auto">
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <img src="http://placehold.it/900x650" alt=""/>
                                        </div>
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 border-all border-width-2 border-color-extra-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-medium font-weight-600 alt-font text-uppercase margin-10px-bottom display-block text-extra-dark-gray">03. Production</span>
                                                    <span class="alt-font text-medium-gray text-medium">Lorem Ipsum is simply dummy text of the printing and industry. Lorem Ipsum typesetting has dummy text.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-left">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 bg-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-extra-dark-gray alt-font margin-10px-bottom display-inline-block text-medium">Delivering beautiful products</span>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text.</p>
                                                    <a href="about-us-classic.html" class="btn btn-dark-gray btn-very-small text-extra-small">About Company</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                            <!-- start tab content -->
                            <div id="tab6_sec4" class="center-col tab-pane fade in"> 
                                <div class="tab-pane fade in"> 
                                    <div class="equalize sm-equalize-auto">
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <img src="http://placehold.it/900x650" alt=""/>
                                        </div>
                                        <div class="col-md-4 text-left sm-margin-30px-bottom">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 border-all border-width-2 border-color-extra-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-medium font-weight-600 alt-font text-uppercase margin-10px-bottom display-block text-extra-dark-gray">04. Happy Client</span>
                                                    <span class="alt-font text-medium-gray text-medium">Lorem Ipsum is simply dummy text of the printing and industry. Lorem Ipsum typesetting has dummy text.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-left">
                                            <div class="position-relative padding-45px-all sm-padding-30px-all height-100 width-100 bg-light-gray display-table">
                                                <div class="display-table-cell vertical-align-middle">
                                                    <span class="text-extra-dark-gray alt-font margin-10px-bottom display-inline-block text-medium">We believe in growth</span>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text.</p>
                                                    <a href="about-us-classic.html" class="btn btn-dark-gray btn-very-small text-extra-small">About Company</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end tab content -->
                        </div>
                        <!-- end tab content section -->
                    </div>
                    <!-- end tab -->
                </div>
            </div>
        </section>
        <!-- end tabs section -->        
        <!-- start information section -->
        <section class="bg-light-gray">
            <div class="container">
                <div class="row equalize sm-equalize-auto">
                    <div class="col-md-5 col-sm-12 sm-margin-30px-bottom wow fadeInLeft">
                        <div class="display-table width-100 heidth-100">
                            <div class="display-table-cell vertical-align-middle">
                                <img src="http://placehold.it/558x675" class="width-100" alt=""/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-md-offset-1 wow fadeInRight">
                        <div class="display-table width-100 heidth-100">
                            <div class="display-table-cell vertical-align-middle">
                                <span class="text-medium text-deep-pink alt-font margin-10px-bottom display-inline-block">Easy way to build perfect websites</span>
                                <h5 class="alt-font text-extra-dark-gray font-weight-600">Beautifully handcrafted templates for your website</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. Lorem Ipsum has been the industry's standard dummy text ever since. Lorem Ipsum is simply dummy text.</p>
                                <div class="margin-30px-tb">
                                    <ul class="no-padding list-style-5">
                                        <li>Beautiful and easy to understand UI, professional animations</li>
                                        <li>Theme advantages are pixel perfect design &amp; clear code delivered</li>
                                        <li>Present your services with flexible, convenient and multipurpose</li>
                                        <li>Find more creative ideas for your projects </li>
                                        <li>Unlimited power and customization possibilities</li> 
                                    </ul>                                
                                </div>
                                <a href="services-modern.html" class="btn btn-dark-gray btn-small text-extra-small">View more info</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end information box section  -->
        <!-- start pricing box section  -->
        <section class="wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="pricing-box-style1">
                        <!-- start price item -->
                        <div class="col-md-4 col-sm-4 col-xs-12 text-center sm-margin-30px-bottom wow fadeInUp">
                            <div class="pricing-box border-all border-width-1 border-color-extra-light-gray">
                                <div class="padding-55px-all bg-very-light-gray sm-padding-30px-all xs-padding-40px-all">
                                    <!-- start price title -->
                                    <div class="pricing-title text-center">
                                        <i class="ti-user icon-large text-deep-pink display-inline-block padding-30px-all bg-white box-shadow-light border-radius-100 margin-25px-bottom"></i>
                                    </div>
                                    <!-- end price title -->
                                    <!-- start price price -->
                                    <div class="pricing-price">
                                        <span class="alt-font text-extra-dark-gray font-weight-600 text-uppercase">Standard</span>
                                        <h4 class="text-extra-dark-gray alt-font font-weight-600 no-margin-bottom">$250</h4>
                                        <div class="text-extra-small text-uppercase alt-font margin-5px-top">Per Month</div>
                                    </div>
                                    <!-- end price price -->
                                </div>
                                <!-- start price features -->
                                <div class="padding-45px-all pricing-features sm-padding-20px-all xs-padding-30px-all">
                                    <ul class="list-style-11">
                                        <li>1 GB Photos</li>
                                        <li>Secure Online Transfer</li>
                                        <li>Unlimited Styles</li>
                                        <li>Customer Service</li>
                                        <li>Manual Backup</li>
                                    </ul>
                                    <!-- start price action -->
                                    <div class="pricing-action margin-35px-top sm-no-margin-top">
                                        <a href="contact-us-simple.html" class="btn btn-dark-gray btn-small text-extra-small">Choose Plan</a>                                        
                                    </div>
                                    <!-- end price action -->
                                </div>
                                <!-- end price features -->
                            </div>
                        </div>
                        <!-- end price item -->
                        <!-- start price item -->
                        <div class="col-md-4 col-sm-4 col-xs-12 text-center sm-margin-30px-bottom wow fadeInUp" data-wow-delay="0.2s">
                            <div class="pricing-box border-all border-width-1 border-color-extra-light-gray">
                                <div class="padding-55px-all bg-very-light-gray sm-padding-30px-all xs-padding-40px-all">
                                    <!-- start price title -->
                                    <div class="pricing-title text-center">
                                        <i class="ti-briefcase icon-large text-deep-pink display-inline-block padding-30px-all bg-white box-shadow-light border-radius-100 margin-25px-bottom"></i>
                                    </div>
                                    <!-- end price title -->
                                    <!-- start price price -->
                                    <div class="pricing-price">
                                        <span class="alt-font text-extra-dark-gray font-weight-600 text-uppercase">Business</span>
                                        <h4 class="text-extra-dark-gray alt-font font-weight-600 no-margin-bottom">$350</h4>
                                        <div class="text-extra-small text-uppercase alt-font margin-5px-top">Per Month</div>
                                    </div>
                                    <!-- end price price -->
                                </div>
                                <!-- start price features -->
                                <div class="padding-45px-all pricing-features sm-padding-20px-all xs-padding-30px-all">
                                    <ul class="list-style-11">
                                        <li>2 GB Photos</li>
                                        <li>Secure Online Transfer</li>
                                        <li>Unlimited Styles</li>
                                        <li>Customer Service</li>
                                        <li>Manual Backup</li>
                                    </ul>
                                    <!-- start price action -->
                                    <div class="pricing-action margin-35px-top sm-no-margin-top">
                                        <a href="contact-us-simple.html" class="btn btn-dark-gray btn-small text-extra-small">Choose Plan</a>                                        
                                    </div>
                                    <!-- end price action -->
                                </div>
                                <!-- end price features -->
                            </div>
                        </div>
                        <!-- end price item -->
                        <!-- start price item -->
                        <div class="col-md-4 col-sm-4 col-xs-12 text-center wow fadeInUp" data-wow-delay="0.4s">
                            <div class="pricing-box border-all border-width-1 border-color-extra-light-gray">
                                <div class="padding-55px-all bg-very-light-gray sm-padding-30px-all xs-padding-40px-all">
                                    <!-- start price title -->
                                    <div class="pricing-title text-center">
                                        <i class="ti-world icon-large text-deep-pink display-inline-block padding-30px-all bg-white box-shadow-light border-radius-100 margin-25px-bottom"></i>
                                    </div>
                                    <!-- end price title -->
                                    <!-- start price price -->
                                    <div class="pricing-price">
                                        <span class="alt-font text-extra-dark-gray font-weight-600 text-uppercase">Ultimate</span>
                                        <h4 class="text-extra-dark-gray alt-font font-weight-600 no-margin-bottom">$450</h4>
                                        <div class="text-extra-small text-uppercase alt-font margin-5px-top">Per Month</div>
                                    </div>
                                    <!-- end price price -->
                                </div>
                                <!-- start price features -->
                                <div class="padding-45px-all pricing-features sm-padding-20px-all xs-padding-30px-all">
                                    <ul class="list-style-11">
                                        <li>Unlimited Photos</li>
                                        <li>Secure Online Transfer</li>
                                        <li>Unlimited Styles</li>
                                        <li>Customer Service</li>
                                        <li>Auto Backup</li>
                                    </ul>
                                    <!-- start price action -->
                                    <div class="pricing-action margin-35px-top sm-no-margin-top">
                                        <a href="contact-us-simple.html" class="btn btn-dark-gray btn-small text-extra-small">Choose Plan</a>                                        
                                    </div>
                                    <!-- end price action -->
                                </div>
                                <!-- end price features -->
                            </div>
                        </div>
                        <!-- end price item -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end price box section -->  
        <!-- start section -->
        <section class="no-padding-bottom bg-light-gray wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 center-col margin-eight-bottom text-center last-paragraph-no-margin">
                        <div class="alt-font text-deep-pink margin-10px-bottom text-uppercase text-small">We combine design, thinking and craft</div>
                        <h5 class="alt-font text-extra-dark-gray font-weight-600">Beautiful and easy to use UI, professional animations and drag & drop feature</h5>
                        <p class="center-col display-inline-block">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="http://placehold.it/1100x523" alt=""/>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section  -->
        <!-- start footer -->
        <?php include 'php-components/footer.php';?>
        <!-- end footer -->
        <!-- start scroll to top -->
        <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
        <!-- end scroll to top  -->
        <!-- javascript libraries -->
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/modernizr.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/skrollr.min.js"></script>
        <script type="text/javascript" src="js/smooth-scroll.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <!-- menu navigation -->
        <script type="text/javascript" src="js/bootsnav.js"></script>
        <script type="text/javascript" src="js/jquery.nav.js"></script>
        <!-- animation -->
        <script type="text/javascript" src="js/wow.min.js"></script>
        <!-- page scroll -->
        <script type="text/javascript" src="js/page-scroll.js"></script>
        <!-- swiper carousel -->
        <script type="text/javascript" src="js/swiper.min.js"></script>
        <!-- counter -->
        <script type="text/javascript" src="js/jquery.count-to.js"></script>
        <!-- parallax -->
        <script type="text/javascript" src="js/jquery.stellar.js"></script>
        <!-- magnific popup -->
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <!-- portfolio with shorting tab -->
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <!-- images loaded -->
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <!-- pull menu -->
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/hamburger-menu.js"></script>
        <!-- counter  -->
        <script type="text/javascript" src="js/counter.js"></script>
        <!-- fit video  -->
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <!-- equalize -->
        <script type="text/javascript" src="js/equalize.min.js"></script>
        <!-- skill bars  -->
        <script type="text/javascript" src="js/skill.bars.jquery.js"></script> 
        <!-- justified gallery  -->
        <script type="text/javascript" src="js/justified-gallery.min.js"></script>
        <!--pie chart-->
        <script type="text/javascript" src="js/jquery.easypiechart.min.js"></script>
        <!-- instagram -->
        <script type="text/javascript" src="js/instafeed.min.js"></script>
        <!-- retina -->
        <script type="text/javascript" src="js/retina.min.js"></script>
        <!-- revolution -->
        <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- revolution slider extensions (load below extensions JS files only on local file systems to make the slider work! The following part can be removed on server for on demand loading) -->
        <!--<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>-->
        <!-- setting -->
        <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>