<header>
		<!-- start navigation -->
		<nav class="navbar navbar-default bootsnav bg-white navbar-top nav-box-width header-light">
			<div class="container nav-header-container">
				<div class="row">
					<!-- start logo -->
					<div class="col-md-2 col-xs-5">
						<a href="/" title="CloudSuite" class="logo">
							<img src="images/horizontal-logo.png" data-rjs="images/logo@2x.png" class="logo-dark default" alt="CloudSuite" style="margin-top:0px;">
							<img src="images/logo-white.png" data-rjs="images/logo-white@2x.png" alt="Pofo" class="logo-light">
						</a>
					</div>
					<!-- end logo -->
                        <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                            <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                                <span class="sr-only">toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                                <ul id="accordion" class="nav navbar-nav navbar-left no-margin alt-font text-normal" data-in="fadeIn" data-out="fadeOut">                                    
                                    <li class="dropdown simple-dropdown">
										<a href="/websites.php">Websites</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>                                        
                                    </li>
                                    <li class="dropdown megamenu-fw">
										<a href="javascript:void(0);">Software Solutions</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>                                       
                                    </li>
									<li class="dropdown megamenu-fw">
										<a href="javascript:void(0);">Mobile Apps</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>                                        
                                    </li>
                                    <li class="dropdown simple-dropdown">
										<a href="javascript:void(0);" title="Blog">Strategic Consulting</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>                                        
                                    </li>
                                    <li class="dropdown megamenu-fw">
										<a href="/about-us.php">About Us</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>                                        
                                    </li>
									<li class="dropdown megamenu-fw">
										<a href="/contact-us.php">Contact Us</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
					<div class="col-md-2 col-xs-5 width-auto">
						<div class="heder-menu-button sm-display-none">
							<button class="navbar-toggle mobile-toggle right-menu-button" type="button" id="showRightPush">
								<span></span>
								<span></span>
								<span></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</nav>
		<!-- end navigation --> 
	</header>