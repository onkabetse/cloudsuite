<footer class="footer-standard-dark bg-extra-dark-gray"> 
	<div class="footer-widget-area padding-five-tb xs-padding-30px-tb">
		<div class="container">
			<div class="row equalize xs-equalize-auto">
				<div class="col-md-3 col-sm-6 col-xs-12 widget border-right border-color-medium-dark-gray sm-no-border-right sm-margin-30px-bottom xs-text-center">
					<!-- start logo -->
					<a href="/" class="margin-20px-bottom display-inline-block"><img class="footer-logo" src="/images/small-clousuite-logo.png" data-rjs="images/small-clousuite-logo@2x.png" alt="CloudSuite"></a>
					<!-- end logo -->
					<p class="text-small width-95 xs-width-100">We change the way local companies interact with technology. Let technology drive your business goals forward.</p>
					
				</div>
				<!-- start contact information -->
				<div class="col-md-5 col-sm-6 col-xs-12 widget border-right border-color-medium-dark-gray padding-45px-left sm-padding-15px-left sm-clear-both sm-no-border-right  xs-margin-30px-bottom xs-text-center">
					<div class="widget-title alt-font text-small text-medium-gray text-uppercase margin-10px-bottom font-weight-600">Contact Info</div>
					<p class="text-small display-block margin-15px-bottom width-80 xs-center-col">CloudSuite (Pty) Ltd<br> Matlakana Estate, Unit 2<br>Plot 116, Tlokweng<br>Botswana</p>
					<div class="text-small">Email: <a href="mailto:sales@domain.com">support@cloudsuite.co.bw</a></div>
					<div class="text-small">Phone: +267 74 225 564</div>
				</div>
				<!-- end contact information -->						
				<!-- start additional links -->
				<div class="col-md-4 col-sm-6 col-xs-12 widget border-right border-color-medium-dark-gray padding-45px-left sm-padding-15px-left sm-no-border-right sm-margin-30px-bottom xs-text-center">
					<div class="widget-title alt-font text-small text-medium-gray text-uppercase margin-10px-bottom font-weight-600">Interesting Links</div>
					<ul class="list-unstyled">
						<li><a class="text-small" href="https://www.weforum.org/agenda/archive/fourth-industrial-revolution">4th Industrial Revolution</a></li>
						<li><a class="text-small" href="https://www.bbc.co.uk/programmes/b006m9ry">BBC Click</a></li>
					</ul>
				</div>
				<!-- end additional links -->
			</div>
		</div>
	</div>
	<div class="bg-dark-footer padding-50px-tb text-center xs-padding-30px-tb">
		<div class="container">
			<div class="row">
				<!-- start copyright -->
				<div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">&copy; 2018 Proudly Powered by <a href="http://www.cloudsuite.co.bw" class="text-dark-gray">CloudSuite</a></div>                        
				<!-- end copyright -->
			</div>
		</div>
	</div>
</footer>